﻿#[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Media")
[void][System.Reflection.Assembly]::Load(“PresentationCore, Version=3.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35”)

$Script:audioManager = New-Object System.Windows.Media.MediaPlayer
$Script:fileOpened = $false
$Script:fileEnded = $false
$Script:fileLastFilePath = ""

$audioManagerMediaEnded = 
{
    $Script:fileEnded = $true
}

$Script:audioManager.Add_MediaEnded($audioManagerMediaEnded)

##########################################
############## AUDIO UTILS ###############
##########################################

#Otwarcie pliku audio i ustawienie odpowiednich zmiennych stanu odtwarzania
Function Open-Audio([String] $filePath)
{
    $Script:audioManager.Open($filePath) > $null
    $Script:fileOpened = $true
    $Script:fileEnded = $false
    $Script:fileLastFilePath = $filePath
}

#Rozpoczęcie / wznowienie odtwarzania pliku
Function Play-Audio([String] $filePath)
{
    if($filePath -and ($filePath -ne $Script:fileLastFilePath))
    {
        Open-Audio $filePath
        $Script:audioManager.Play() > $null
    }
    elseif($Script:fileOpened)
    {
        if($Script:fileEnded)
        {
            Open-Audio $Script:fileLastFilePath
            $Script:audioManager.Play() > $null
        }
        else
        {
            $Script:audioManager.Play() > $null
        }
    }
}

#Spauzowanie odtwarzania
Function Pause-Audio()
{
    if($Script:fileOpened)
    {
        $Script:audioManager.Pause() > $null
    }
}
#Zatrzymanie odtwarzania
Function Stop-Audio()
{
    if($Script:fileOpened)
    {
        $Script:audioManager.Stop() > $null
    }
}

#Pobranie względnej pozycji utworu w granicach 0.0 - 1.0
Function Get-RelativePosition()
{
	$pos = 0
	if($Script:audioManager.NaturalDuration.TimeSpan)
	{
		$pos = $Script:audioManager.Position.TotalMilliseconds / $Script:audioManager.NaturalDuration.TimeSpan.TotalMilliseconds
	}
	
    if($pos -ge 1)
    {
        $Script:fileEnded = $True
    }
    return $pos
}

#Ustawienie względnej pozycji utworu w granicach 0.0 - 1.0
Function Set-RelativePosition([Double] $newPosition)
{
	$pos = 0
    if($newPosition -lt 0)
    {
        $newPosition = 0
    }
    elseif($newPositio -gt 1)
    {
        $newPosition = 1.0
    }
    $newPos = $newPosition * $Script:audioManager.NaturalDuration.TimeSpan.TotalMilliseconds
    $Script:audioManager.Position = [System.TimeSpan]::FromMilliseconds($newPos)
}

#Pobranie napisu opisującego pozycję utworu w stosunku do całości w formacie mm:ss/mm:ss
Function Get-StringPosition()
{
	$duration = "00:00"
	if($Script:audioManager.NaturalDuration.TimeSpan)
	{
		$duration = "{0:mm:ss}" -f ([datetime]$Script:audioManager.NaturalDuration.TimeSpan.Ticks)
	}
    
    $position = "{0:mm:ss}" -f ([datetime]$Script:audioManager.Position.Ticks)
    
    return $position + "/" + $duration
}

#Zmienna tylko dla funkcji Get-FileDuration
$mediaPlayerDuration = New-Object System.Windows.Media.MediaPlayer

#Pobranie czasu trwania utworu
Function Get-FileDuration([String]$filePath)
{
    $mediaPlayerDuration.Open($filePath)
    $i = 0;
    $maxCount = 120000;
    #Tuż po otwarciu pliku czas trwania nie jest dostępny, dlatego czekamy na spełnienie HasTimeSpan.
    #Dla plików nie multimedialnych HasTimeSpan nigdy nie będzie spełniony stąd drugi warunek zabezpieczający przed zawieszeniem.
    while((!$mediaPlayerDuration.NaturalDuration.HasTimeSpan) -and ($i -le $maxCount)) 
    {
        $i++
    }
    return "{0:mm:ss}" -f ([datetime]$mediaPlayerDuration.NaturalDuration.TimeSpan.Ticks)
}
