﻿#[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing") 
#[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") 
[void][System.Reflection.Assembly]::Load(“System.Drawing, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a”) 
[void][System.Reflection.Assembly]::Load(“System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089”) 

# Włączenie nowoczesnego wyglądu kontrolek
[System.Windows.Forms.Application]::EnableVisualStyles()

# Wczytanie grafik z zasobów
$filePlay = (get-item '.\res\play.png')
$imgPlay = [System.Drawing.Image]::Fromfile($filePlay)
$filePause = (get-item '.\res\pause.png')
$imgPause = [System.Drawing.Image]::Fromfile($filePause)
$fileStop = (get-item '.\res\stop.png')
$imgStop = [System.Drawing.Image]::Fromfile($fileStop)
$filePrevious = (get-item '.\res\previous.png')
$imgPrevious = [System.Drawing.Image]::Fromfile($filePrevious)
$fileNext = (get-item '.\res\next.png')
$imgNext = [System.Drawing.Image]::Fromfile($fileNext)

# Tworzenie obiektów
$windowForm =  New-Object System.Windows.Forms.Form
$mainTableLayout = New-Object System.Windows.Forms.TableLayoutPanel
$playButton = New-Object System.Windows.Forms.Button
$pauseButton = New-Object System.Windows.Forms.Button
$stopButton = New-Object System.Windows.Forms.Button
$previousButton = New-Object System.Windows.Forms.Button
$nextButton = New-Object System.Windows.Forms.Button
$trackName = New-Object System.Windows.Forms.Label
$trackTableLayout = New-Object System.Windows.Forms.TableLayoutPanel
$trackList = New-Object System.Windows.Forms.ListView
$TableLayoutPanel1 = New-Object System.Windows.Forms.TableLayoutPanel
$TableLayoutPanel2 = New-Object System.Windows.Forms.TableLayoutPanel
$rootPathTextBox = New-Object System.Windows.Forms.TextBox
$chooseRootPathButton = New-Object System.Windows.Forms.Button
$dirTree = New-Object System.Windows.Forms.TreeView
$trackPosition = New-Object System.Windows.Forms.TrackBar
$TableLayoutPanel3 = New-Object System.Windows.Forms.TableLayoutPanel
$trackPositionLabel = New-Object System.Windows.Forms.Label
$TableLayoutPanel4 = New-Object System.Windows.Forms.TableLayoutPanel

#
#mainTableLayout
#
$mainTableColimnStyle1 = New-Object System.Windows.Forms.ColumnStyle
$mainTableColimnStyle1.SizeType = [System.Windows.Forms.SizeType]::Percent
$mainTableColimnStyle1.Width = 100.0

$mainTableRowStyle1 = New-Object System.Windows.Forms.RowStyle
$mainTableRowStyle1.SizeType = [System.Windows.Forms.SizeType]::Absolute
$mainTableRowStyle1.Height = 45.0

$mainTableRowStyle2 = New-Object System.Windows.Forms.RowStyle
$mainTableRowStyle2.SizeType = [System.Windows.Forms.SizeType]::Absolute
$mainTableRowStyle2.Height = 25.0

$mainTableRowStyle3 = New-Object System.Windows.Forms.RowStyle

$mainTableRowStyle4 = New-Object System.Windows.Forms.RowStyle
$mainTableRowStyle4.SizeType = [System.Windows.Forms.SizeType]::Absolute
$mainTableRowStyle4.Height = 20.0

$mainTableLayout.ColumnCount = 1
$mainTableLayout.ColumnStyles.Add($mainTableColimnStyle1) > $null
$mainTableLayout.Controls.Add($trackTableLayout, 0, 2) > $null
$mainTableLayout.Controls.Add($TableLayoutPanel3, 0, 1) > $null
$mainTableLayout.Controls.Add($TableLayoutPanel4, 0, 0) > $null
$mainTableLayout.Dock = [System.Windows.Forms.DockStyle]::Fill
$mainTableLayout.Location = New-Object System.Drawing.Point(0, 0)
$mainTableLayout.Name = "mainTableLayout"
$mainTableLayout.RowCount = 3
$mainTableLayout.RowStyles.Add($mainTableRowStyle1) > $null
$mainTableLayout.RowStyles.Add($mainTableRowStyle2) > $null
$mainTableLayout.RowStyles.Add($mainTableRowStyle3) > $null
$mainTableLayout.RowStyles.Add($mainTableRowStyle4) > $null
$mainTableLayout.Size = New-Object System.Drawing.Size(879, 439)
$mainTableLayout.TabIndex = 0
#
#playButton
#
$playButton.AccessibleName = "playButton"
$playButton.Dock = [System.Windows.Forms.DockStyle]::Fill
$playButton.Image = $imgPlay
$playButton.Location = New-Object System.Drawing.Point(3, 3)
$playButton.Name = "playButton"
$playButton.Size = New-Object System.Drawing.Size(39, 39)
$playButton.TabIndex = 0
$playButton.UseVisualStyleBackColor = $True
#
#pauseButton
#
$pauseButton.AccessibleName = "pauseButton"
$pauseButton.Dock = [System.Windows.Forms.DockStyle]::Fill
$pauseButton.Image = $imgPause
$pauseButton.Location = New-Object System.Drawing.Point(48, 3)
$pauseButton.Name = "pauseButton"
$pauseButton.Size = New-Object System.Drawing.Size(39, 39)
$pauseButton.TabIndex = 1
$pauseButton.UseVisualStyleBackColor = $True
#
#stopButton
#
$stopButton.AccessibleName = "stopButton"
$stopButton.Dock = [System.Windows.Forms.DockStyle]::Fill
$stopButton.Image = $imgStop
$stopButton.Location = New-Object System.Drawing.Point(93, 3)
$stopButton.Name = "stopButton"
$stopButton.Size = New-Object System.Drawing.Size(39, 39)
$stopButton.TabIndex = 2
$stopButton.UseVisualStyleBackColor = $True
#
#previousButton
#
$previousButton.AccessibleName = "previousButton"
$previousButton.Dock = [System.Windows.Forms.DockStyle]::Fill
$previousButton.Image = $imgPrevious
$previousButton.Location = New-Object System.Drawing.Point(138, 3)
$previousButton.Name = "previousButton"
$previousButton.Size = New-Object System.Drawing.Size(39, 39)
$previousButton.TabIndex = 3
$previousButton.UseVisualStyleBackColor = $True
#
#nextButton
#
$nextButton.AccessibleName = "nextButton"
$nextButton.Dock = [System.Windows.Forms.DockStyle]::Fill
$nextButton.Image = $imgNext
$nextButton.Location = New-Object System.Drawing.Point(183, 3)
$nextButton.Name = "nextButton"
$nextButton.Size = New-Object System.Drawing.Size(39, 39)
$nextButton.TabIndex = 4
$nextButton.UseVisualStyleBackColor = $True
#
#trackName
#
$trackName.AutoSize = $True
$trackName.Dock = [System.Windows.Forms.DockStyle]::Fill
$trackName.Location = New-Object System.Drawing.Point(228, 0)
$trackName.Name = "trackName"
$trackName.Size = New-Object System.Drawing.Size(648, 45)
$trackName.TabIndex = 5
$trackName.Text = "Title"
$trackName.TextAlign = [System.Drawing.ContentAlignment]::MiddleCenter
#
#trackTableLayout
#
$trackTableColumnStyle1 = New-Object System.Windows.Forms.ColumnStyle
$trackTableColumnStyle1.SizeType = [System.Windows.Forms.SizeType]::Percent
$trackTableColumnStyle1.Width = 30.0

$trackTableColumnStyle2 = New-Object System.Windows.Forms.ColumnStyle
$trackTableColumnStyle2.SizeType = [System.Windows.Forms.SizeType]::Percent
$trackTableColumnStyle2.Width = 70.0

$trackTableRowStyle1 = New-Object System.Windows.Forms.RowStyle
$trackTableRowStyle1.SizeType = [System.Windows.Forms.SizeType]::Percent
$trackTableRowStyle1.Height = 100

$trackTableLayout.ColumnCount = 2
$trackTableLayout.ColumnStyles.Add($trackTableColumnStyle1) > $null
$trackTableLayout.ColumnStyles.Add($trackTableColumnStyle2) > $null
$trackTableLayout.Controls.Add($trackList, 1, 0) > $null
$trackTableLayout.Controls.Add($TableLayoutPanel1, 0, 0) > $null
$trackTableLayout.Dock = [System.Windows.Forms.DockStyle]::Fill
$trackTableLayout.Location = New-Object System.Drawing.Point(3, 73)
$trackTableLayout.Name = "trackTableLayout"
$trackTableLayout.RowCount = 1
$trackTableLayout.RowStyles.Add($trackTableRowStyle1) > $null
$trackTableLayout.Size = New-Object System.Drawing.Size(873, 363)
$trackTableLayout.TabIndex = 1
#
#trackList
#
$trackList.AccessibleName = "trackList"
$trackList.Dock = [System.Windows.Forms.DockStyle]::Fill
$trackList.Location = New-Object System.Drawing.Point(264, 3)
$trackList.MultiSelect = $False
$trackList.Name = "trackList"
$trackList.Size = New-Object System.Drawing.Size(606, 357)
$trackList.TabIndex = 1
$trackList.UseCompatibleStateImageBehavior = $False
$trackList.View = [System.Windows.Forms.View]::Details
$trackList.AllowColumnReorder = True
$trackList.Columns.Add("Path", 500) > $null
$trackList.Columns.Add("Duration", 100) > $null
#
#TableLayoutPanel1
#
$TableLayoutPanel1ColumnStyle1 = New-Object System.Windows.Forms.ColumnStyle
$TableLayoutPanel1ColumnStyle1.SizeType = [System.Windows.Forms.SizeType]::Percent
$TableLayoutPanel1ColumnStyle1.Width = 100.0

$TableLayoutPanel1RowStyle1 = New-Object System.Windows.Forms.RowStyle
$TableLayoutPanel1RowStyle1.SizeType = [System.Windows.Forms.SizeType]::Absolute
$TableLayoutPanel1RowStyle1.Height = 30

$TableLayoutPanel1RowStyle2 = New-Object System.Windows.Forms.RowStyle
$TableLayoutPanel1RowStyle2.SizeType = [System.Windows.Forms.SizeType]::Percent
$TableLayoutPanel1RowStyle2.Height = 100

$TableLayoutPanel1.ColumnCount = 1
$TableLayoutPanel1.ColumnStyles.Add($TableLayoutPanel1ColumnStyle1) > $null
$TableLayoutPanel1.Controls.Add($TableLayoutPanel2, 0, 0) > $null
$TableLayoutPanel1.Controls.Add($dirTree, 0, 1) > $null
$TableLayoutPanel1.Dock = [System.Windows.Forms.DockStyle]::Fill
$TableLayoutPanel1.Location = New-Object System.Drawing.Point(3, 3)
$TableLayoutPanel1.Name = "TableLayoutPanel1"
$TableLayoutPanel1.RowCount = 2
$TableLayoutPanel1.RowStyles.Add($TableLayoutPanel1RowStyle1) > $null
$TableLayoutPanel1.RowStyles.Add($TableLayoutPanel1RowStyle2) > $null
$TableLayoutPanel1.Size = New-Object System.Drawing.Size(255, 357)
$TableLayoutPanel1.TabIndex = 2
#
#TableLayoutPanel2
#
$TableLayoutPanel2ColumnStyle1 = New-Object System.Windows.Forms.ColumnStyle
$TableLayoutPanel2ColumnStyle1.SizeType = [System.Windows.Forms.SizeType]::Percent
$TableLayoutPanel2ColumnStyle1.Width = 100.0

$TableLayoutPanel2ColumnStyle2 = New-Object System.Windows.Forms.ColumnStyle
$TableLayoutPanel2ColumnStyle2.SizeType = [System.Windows.Forms.SizeType]::Absolute
$TableLayoutPanel2ColumnStyle2.Width = 50.0

$TableLayoutPanel2RowStyle1 = New-Object System.Windows.Forms.RowStyle
$TableLayoutPanel2RowStyle1.SizeType = [System.Windows.Forms.SizeType]::Percent
$TableLayoutPanel2RowStyle1.Height = 100

$TableLayoutPanel2.ColumnCount = 2
$TableLayoutPanel2.ColumnStyles.Add($TableLayoutPanel2ColumnStyle1) > $null
$TableLayoutPanel2.ColumnStyles.Add($TableLayoutPanel2ColumnStyle2) > $null
$TableLayoutPanel2.Controls.Add($rootPathTextBox, 0, 0) > $null
$TableLayoutPanel2.Controls.Add($chooseRootPathButton, 1, 0) > $null
$TableLayoutPanel2.Dock = [System.Windows.Forms.DockStyle]::Fill
$TableLayoutPanel2.Location = New-Object System.Drawing.Point(0, 0)
$TableLayoutPanel2.Margin = New-Object System.Windows.Forms.Padding(0)
$TableLayoutPanel2.Name = "TableLayoutPanel2"
$TableLayoutPanel2.RowCount = 1
$TableLayoutPanel2.RowStyles.Add($TableLayoutPanel2RowStyle1) > $null
$TableLayoutPanel2.Size = New-Object System.Drawing.Size(255, 30)
$TableLayoutPanel2.TabIndex = 1
#
#rootPathTextBox
#
$rootPathTextBox.Dock = [System.Windows.Forms.DockStyle]::Fill
$rootPathTextBox.Location = New-Object System.Drawing.Point(3, 3)
$rootPathTextBox.Name = "rootPathTextBox"
$rootPathTextBox.ReadOnly = $True
$rootPathTextBox.Size = New-Object System.Drawing.Size(199, 20)
$rootPathTextBox.TabIndex = 0
$rootPathTextBox.Text = "Choose path ->"
#
#chooseRootPathButton
#
$chooseRootPathButton.Dock = [System.Windows.Forms.DockStyle]::Fill
$chooseRootPathButton.Location = New-Object System.Drawing.Point(208, 3)
$chooseRootPathButton.Name = "chooseRootPathButton"
$chooseRootPathButton.Size = New-Object System.Drawing.Size(44, 24)
$chooseRootPathButton.TabIndex = 1
$chooseRootPathButton.Text = "..."
$chooseRootPathButton.UseVisualStyleBackColor = $True
#
#dirTree
#
$dirTree.AccessibleName = "dirTree"
$dirTree.Dock = [System.Windows.Forms.DockStyle]::Fill
$dirTree.Location = New-Object System.Drawing.Point(3, 33)
$dirTree.Name = "dirTree"
$dirTree.Size = New-Object System.Drawing.Size(249, 321)
$dirTree.TabIndex = 0
#
#trackPosition
#
$trackPosition.AllowDrop = $True
$trackPosition.Dock = [System.Windows.Forms.DockStyle]::Fill
$trackPosition.Enabled = $True
$trackPosition.LargeChange = 2
$trackPosition.Location = New-Object System.Drawing.Point(3, 3)
$trackPosition.Maximum = 2000
$trackPosition.Name = "trackPosition"
$trackPosition.Size = New-Object System.Drawing.Size(807, 45)
$trackPosition.TabIndex = 2
$trackPosition.TabStop = $False
$trackPosition.TickStyle = [System.Windows.Forms.TickStyle]::None
$trackPosition.Value = 0
#
#TableLayoutPanel3
#
$TableLayoutPanel3ColumnStyle1 = New-Object System.Windows.Forms.ColumnStyle
$TableLayoutPanel3ColumnStyle1.SizeType = [System.Windows.Forms.SizeType]::Percent
$TableLayoutPanel3ColumnStyle1.Width = 100.0

$TableLayoutPanel3ColumnStyle2 = New-Object System.Windows.Forms.ColumnStyle
$TableLayoutPanel3ColumnStyle2.SizeType = [System.Windows.Forms.SizeType]::Absolute
$TableLayoutPanel3ColumnStyle2.Width = 80.0

$TableLayoutPanel3RowStyle1 = New-Object System.Windows.Forms.RowStyle

$TableLayoutPanel3.ColumnCount = 2
$TableLayoutPanel3.ColumnStyles.Add($TableLayoutPanel3ColumnStyle1) > $null
$TableLayoutPanel3.ColumnStyles.Add($TableLayoutPanel3ColumnStyle2) > $null
$TableLayoutPanel3.Controls.Add($trackPosition, 0, 0) > $null
$TableLayoutPanel3.Controls.Add($trackPositionLabel, 1, 0) > $null
$TableLayoutPanel3.Dock = [System.Windows.Forms.DockStyle]::Fill
$TableLayoutPanel3.Location = New-Object System.Drawing.Point(3, 45)
$TableLayoutPanel3.Margin = New-Object System.Windows.Forms.Padding(3, 0, 3, 0)
$TableLayoutPanel3.Name = "TableLayoutPanel3"
$TableLayoutPanel3.RowCount = 1
$TableLayoutPanel3.RowStyles.Add($TableLayoutPanel3RowStyle1)  > $null
$TableLayoutPanel3.Size = New-Object System.Drawing.Size(873, 25)
$TableLayoutPanel3.TabIndex = 2

#
#trackPositionLabel
#
$trackPositionLabel.AutoSize = $True
$trackPositionLabel.Dock = [System.Windows.Forms.DockStyle]::Fill
$trackPositionLabel.Location = New-Object System.Drawing.Point(816, 0)
$trackPositionLabel.Name = "trackPositionLabel"
$trackPositionLabel.Size = New-Object System.Drawing.Size(54, 51)
$trackPositionLabel.TabIndex = 3
$trackPositionLabel.Text = "00:00/00:00"
$trackPositionLabel.TextAlign = [System.Drawing.ContentAlignment]::TopCenter
#
#TableLayoutPanel4
#
$TableLayoutPanel4ColumnStyle1 = New-Object System.Windows.Forms.ColumnStyle
$TableLayoutPanel4ColumnStyle1.SizeType = [System.Windows.Forms.SizeType]::Absolute
$TableLayoutPanel4ColumnStyle1.Width = 45.0

$TableLayoutPanel4ColumnStyle2 = New-Object System.Windows.Forms.ColumnStyle
$TableLayoutPanel4ColumnStyle2.SizeType = [System.Windows.Forms.SizeType]::Absolute
$TableLayoutPanel4ColumnStyle2.Width = 45.0

$TableLayoutPanel4ColumnStyle3 = New-Object System.Windows.Forms.ColumnStyle
$TableLayoutPanel4ColumnStyle3.SizeType = [System.Windows.Forms.SizeType]::Absolute
$TableLayoutPanel4ColumnStyle3.Width = 45.0

$TableLayoutPanel4ColumnStyle4 = New-Object System.Windows.Forms.ColumnStyle
$TableLayoutPanel4ColumnStyle4.SizeType = [System.Windows.Forms.SizeType]::Absolute
$TableLayoutPanel4ColumnStyle4.Width = 45.0

$TableLayoutPanel4ColumnStyle5 = New-Object System.Windows.Forms.ColumnStyle
$TableLayoutPanel4ColumnStyle5.SizeType = [System.Windows.Forms.SizeType]::Absolute
$TableLayoutPanel4ColumnStyle5.Width = 45.0

$TableLayoutPanel4ColumnStyle6 = New-Object System.Windows.Forms.ColumnStyle

$TableLayoutPanel4RowStyle1 = New-Object System.Windows.Forms.RowStyle
$TableLayoutPanel4RowStyle1.SizeType = [System.Windows.Forms.SizeType]::Percent
$TableLayoutPanel4RowStyle1.Height = 100.0

$TableLayoutPanel4.ColumnCount = 6
$TableLayoutPanel4.ColumnStyles.Add($TableLayoutPanel4ColumnStyle1) > $null
$TableLayoutPanel4.ColumnStyles.Add($TableLayoutPanel4ColumnStyle2) > $null
$TableLayoutPanel4.ColumnStyles.Add($TableLayoutPanel4ColumnStyle3) > $null
$TableLayoutPanel4.ColumnStyles.Add($TableLayoutPanel4ColumnStyle4) > $null
$TableLayoutPanel4.ColumnStyles.Add($TableLayoutPanel4ColumnStyle5) > $null
$TableLayoutPanel4.ColumnStyles.Add($TableLayoutPanel4ColumnStyle6) > $null
$TableLayoutPanel4.Controls.Add($trackName, 5, 0) > $null
$TableLayoutPanel4.Controls.Add($nextButton, 4, 0) > $null
$TableLayoutPanel4.Controls.Add($stopButton, 2, 0) > $null
$TableLayoutPanel4.Controls.Add($pauseButton, 1, 0) > $null
$TableLayoutPanel4.Controls.Add($playButton, 0, 0) > $null
$TableLayoutPanel4.Controls.Add($previousButton, 3, 0) > $null
$TableLayoutPanel4.Dock = [System.Windows.Forms.DockStyle]::Fill
$TableLayoutPanel4.Location = New-Object System.Drawing.Point(0, 0)
$TableLayoutPanel4.Margin = New-Object System.Windows.Forms.Padding(0)
$TableLayoutPanel4.Name = "TableLayoutPanel4"
$TableLayoutPanel4.RowCount = 1
$TableLayoutPanel4.RowStyles.Add($TableLayoutPanel4RowStyle1) > $null
$TableLayoutPanel4.Size = New-Object System.Drawing.Size(879, 45)
$TableLayoutPanel4.TabIndex = 3
#
#AudioPlayer 
#
$windowForm.AutoScaleDimensions =  New-Object System.Drawing.SizeF(6.0, 13.0)
$windowForm.AutoScaleMode = [System.Windows.Forms.AutoScaleMode]::Font
$windowForm.AutoSize = True
$windowForm.ClientSize =  New-Object System.Drawing.Size(879, 439)
$windowForm.MinimumSize =  New-Object System.Drawing.Size(430, 300)
$windowForm.Name = "AudioPlayer"
$windowForm.Text = "AudioPlayer"
$windowForm.Controls.Add($mainTableLayout) > $null
