﻿#Dołączenie skryptu tworzącego GUI
. './scripts/BuildForm.ps1'
#Dołączenie skryptu obsługi audio
. './scripts/Audio.ps1'

##########################################
############ FILE LIST UTILS #############
##########################################

#Dodawanie nowego elementu do drzewa katalogów
Function Add-Node([System.Windows.Forms.TreeNode]$parentNode, [String]$name, [String]$path)
{
    $newNode = New-Object System.Windows.Forms.TreeNode
    $newNode.Text = $name
    $newNode.Tag = $path
    $parentNode.Nodes.Add($newNode) > $null

    return $newNode
}

#Rekurencyjne przejście po wszystkich pod folderach i dodanie ich do drzewa katalogów
Function Get-Dirs([System.Windows.Forms.TreeNode]$parentNode, [String]$path = $pwd)
{
    foreach ($item in Get-ChildItem $path)
    {
        if (Test-Path $item.FullName -PathType Container) #sprawdzenie czy ścieżka jest folderem
        {
            $newNode = Add-Node $parentNode $item.Name $item.FullName
            Get-Dirs $newNode $item.FullName
        }
    }
}

#Zbudowanie drzewa katalogów o zadanym korzeniu
Function Build-DirView([String]$root = $pwd)
{
    $dirTree.Nodes.Clear() > $null

    $rootNode = New-Object System.Windows.Forms.TreeNode
    $rootNode.Text = $root
    $rootNode.Tag = $root

    $dirTree.Nodes.Add($rootNode) > $null

    Get-Dirs $rootNode $root > $null
    $rootNode.Expand() > $null
}

#Ustawienie wybranej ścieżki na text boxie i zbudowanie drzewa katalogów
Function Set-DirRootPath([String]$path = $pwd)
{
    $rootPathTextBox.Text = $path
    Build-DirView $path > $null
}

#Dodanie pliku do listy utworów
Function Add-Track([String]$trackPath)
{
    $duration = Get-FileDuration $trackPath
    $listItem = New-Object Windows.Forms.ListViewItem
    $listItem.Text = $trackPath 
    $durationSubItem =  New-Object System.Windows.Forms.ListViewItem+ListViewSubItem
    $durationSubItem.Text = $duration
    $listItem.SubItems.Add($durationSubItem) > $null

    $trackList.Items.Add($listItem) > $null
}

$audioExtensionArray =".mp3", ".wav", ".3gp", ".aac", ".m4a", ".wma" #prawdopodobnie MediaPlayer obsługuje więcej rozszerzeń
#Tworzenie listy utworów znajdujących się w podanym katalogu
Function Build-TrackList([String]$path)
{
    $trackList.Items.Clear() > $null
    $audioFileList = Get-ChildItem $path
    foreach ($item in $audioFileList)
    {
        if ((Test-Path $item.FullName -PathType Leaf) -and ($audioExtensionArray -contains $item.Extension)) #sprawdzenie czy ścieżka jest plikiem muzycznym
        {
            Add-Track $item.FullName
        }
    }
}

##########################################
############### PLAY UTILS ###############
##########################################

$Global:trackIndex = -1
#Rozpocznij / kontynuuj odgrywanie pliku z listy o danym indeksie (trackIndex)
Function Start-Play()
{
    if(($Global:trackIndex -ge 0) -and ($Global:trackIndex -lt $trackList.Items.Count) )
    {
        $trackName.Text = $trackList.Items[$Global:trackIndex].Text
        Play-Audio $trackList.Items[$Global:trackIndex].Text

        $trackPositionLabel.Text = Get-StringPosition
        Start-Timer
    }
    elseif($trackList.Items -and ($trackList.Items.Count -gt 0))
    {
        $Global:trackIndex = 0;
        $trackName.Text = $trackList.Items[0].Text
        Play-Audio $trackList.Items[0].Text

        $trackPositionLabel.Text = Get-StringPosition
        Start-Timer
    }
}
#Wybierz kolejny utwór z listy i go odtwórz
Function Play-Next()
{
    if($trackList.Items -and ($trackList.Items.Count -gt 0))
    {
        if($Global:trackIndex -ge $trackList.Items.Count)
        {
            $Global:trackIndex = 0;
        }
        else
        {
            $Global:trackIndex++;
        }

        Start-Play
    }
}
#Wybierz poprzedni utwór z listy i go odtwórz
Function Play-Previous()
{
    if($trackList.Items -and ($trackList.Items.Count -gt 0))
    {
        if($Global:trackIndex -le 0)
        {
            $Global:trackIndex = $trackList.Items.Count - 1;
        }
        else
        {
            $Global:trackIndex--;
        }

        Start-Play
    }
}
##########################################
######### POSITION UPDATE TIMER ##########
##########################################

#Zmienna mówiąca czy pozycja utworu jest zmieniana za pomocą myszki
$Global:trackPositionMouseMove = $False;

$positionUpdateTimer = New-Object System.Windows.Forms.Timer
$positionUpdateTimer.Interval = 50
$positionUpdateTimer.add_tick({Update-TrackPosition})

#Pobranie aktualnej pozycji utworu i ustawienie jej na pasku postępu (sliderze) oraz obok niego jako napis
Function Update-TrackPosition()
{
    if($Global:trackPositionMouseMove)
    {
        return
    }

    $trackPositionLabel.Text = Get-StringPosition
    $trackPos = Get-RelativePosition
    if($trackPos -eq 1)
    {
        $trackPosition.Value = 0
        Play-Next
    }
    else
    {
        $trackPos = $trackPos * $trackPosition.Maximum
        $trackPosition.Value = $trackPos
    }
}
#Wystartowanie timera odświerzającego pozycję
function Start-Timer() 
{ 
   $positionUpdateTimer.start() > $null
}
#Zatrzymanie timera odświerzającego pozycję
function Stop-Timer() 
{
    $positionUpdateTimer.Enabled = $false
}

##########################################
################ HANDLERS ################
##########################################

#Handler na wciśnięcie przycisku play
$playButtonClick =
{
    Start-Play
}

#Handler na wciśnięcie przycisku pause
$pauseButtonClick = 
{
    Pause-Audio
    Stop-Timer
}

#Handler na wciśnięcie przycisku stop
$stopButtonClick = 
{
    Stop-Audio
    Stop-Timer
}

#Handler na wciśnięcie przycisku next
$nextButtonClick =
{
    Play-Next
}

#Handler na wciśnięcie przycisku previous
$previousButtonClick =
{
    Play-Previous
}

#Handler na zmianę ścieżki do początku drzewa katalogów
$chooseRootPathButtonClick = 
{
    $folderBrowser = New-Object System.Windows.Forms.FolderBrowserDialog
    [void]$folderBrowser.ShowDialog()

    if($folderBrowser.SelectedPath)
    {
        Set-DirRootPath $folderBrowser.SelectedPath
    }
}

#Handlr na zaznaczenie elementu w drzewie katalogów
$dirTreeAfterSelect = 
{
    #Event Argument: $_ = [System.Windows.Forms.TreeViewEventArgs]
    $Global:trackIndex = -1
    Build-TrackList $_.Node.Tag
}

#Handler na wybranie elementu z listy
$trackListItemActivate =
{
    $Global:trackIndex = $trackList.SelectedItems[0].Index
    Start-Play
}

#Handler na naciśnięcie myszką na pasku pozycji piosenki
$trackPositionMouseDown = 
{
    $Global:trackPositionMouseMove = $True
}

#Handler na puszczenie myszki na pasku pozycji piosenki
$trackPositionMouseUp = 
{
    #Ustawiamy nową pozycję utworu
    $relativePos = $trackPosition.Value / $trackPosition.Maximum
    Set-RelativePosition($relativePos)
    $Global:trackPositionMouseMove = $False
}

#Handler na zamknięcie aplikacji
$windowFormClosing = 
{
    Stop-Audio
}

#Ustawienie handlerów przycisków
$playButton.Add_Click($playButtonClick) > $null
$pauseButton.Add_Click($pauseButtonClick) > $null
$stopButton.Add_Click($stopButtonClick) > $null
$nextButton.Add_Click($nextButtonClick) > $null
$previousButton.Add_Click($previousButtonClick) > $null
$chooseRootPathButton.Add_Click($chooseRootPathButtonClick) > $null

#Ustawienie handlerów drzewa i listy
$dirTree.Add_AfterSelect($dirTreeAfterSelect) > $null
$trackList.Add_ItemActivate($trackListItemActivate) > $null

#Ustawienie handlerów paska pozycji utworu
$trackPosition.Add_MouseDown($trackPositionMouseDown) > $null
$trackPosition.Add_MouseUp($trackPositionMouseUp) > $null

#Ustawienie handlera na zamykanie aplikacji
$windowForm.Add_Closing($windowFormClosing)

#Ustawienie początkowej ścieżki drzewa katalogów
Set-DirRootPath $pwd > $null

#Pokazanie okna aplikacji
$windowForm.ShowDialog()  > $null

